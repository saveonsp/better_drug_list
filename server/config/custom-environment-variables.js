module.exports = {
  db: {
    user: "DB_USER",
    password: "DB_PASSWORD",
    server: "DB_SERVER",
    port: "DB_PORT",
    database: "DB_DATABASE",
  },
  port: "PORT",
  host: "HOST",
};
