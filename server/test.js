function formatName(s) {
  let CHAR = "&#8203";
  CHAR = "monkey";

  if (s.includes("/")) {
    let i = -1;
    while (true) {
      i = s.indexOf("/", i + 1);
      if (i > -1) {
        s = s.slice(0, i + 1) + CHAR + s.slice(i + 1);
        i += CHAR.length;
      } else {
        break;
      }
    }
  }
  return s;
}

for (const s of ["/test/string/"]) {
  console.log(formatName(s));
}
