const fs = require("fs");
const { promises: fsp } = fs;
const path = require("path");
const assert = require("assert").strict;

const express = require("express");
const sql = require("mssql");
process.env["NODE_CONFIG_DIR"] = path.join(__dirname, "config");
const config = require("config");

const CACHE_DIR = path.resolve(process.cwd(), "cache");

(async () => {
  if (!(await isDirectory(CACHE_DIR))) {
    await fsp.mkdir(CACHE_DIR);
  }

  const app = express();

  app.set("view engine", "pug");
  app.set("views", path.join(__dirname, "./views"));
  app.use(express.static(path.join(__dirname, "public")));

  app.db = await sql.connect(config.get("db"));

  // await cacheAllAccountDrugLists(app.db);

  app.get("/", (req, res) => {
    res.redirect("/accounts");
  });

  app.get("/accounts", (req, res) => res.redirect("/accounts.html"));
  app.get("/accounts.:ext", async (req, res) => {
    const { ext } = req.params;

    const request = new sql.Request(req.app.db);
    const result = getAccounts(req.app.db);
    const records = [];
    for await (const row of result) {
      records.push(row);
    }
    switch (ext) {
      case "html":
        res.render("accounts", { records });
        break;
      case "json":
      default:
        res.json(records);
    }
  });

  app.get("/accounts/:id/druglist", (req, res) =>
    res.redirect(`/accounts/${req.params.id}/druglist.html`)
  );
  app.get("/accounts/:id/druglist.:ext", async (req, res, next) => {
    const date = new Date();
    const { id: accountId, ext } = req.params;
    const cacheFileName = path.resolve(CACHE_DIR, `druglist_${accountId}.json`);
    let records;
    const cacheExists = await isFile(cacheFileName);
    console.log(cacheFileName, cacheExists);
    if (cacheExists) {
      const buf = await fsp.readFile(cacheFileName);
      records = JSON.parse(buf.toString());
    } else {
      // drugs.variableCopay,  // <-- hasn't been added to wampdev
      const result = getAccountDrugList(req.app.db, accountId, date);
      records = [];
      for await (const row of result) {
        records.push(row);
      }
      await fsp.writeFile(cacheFileName, JSON.stringify(records));
    }
    switch (ext) {
      case "html":
        const grouped = Object.entries(
          records.reduce((groups, record) => {
            const char = record["drugName"][0];
            (groups[char] || (groups[char] = [])).push(record);
            return groups;
          }, {})
        ).map(([char, items]) => ({ title: char, items }));
        res.setHeader("content-type", "text/html; charset=utf-8");
        res.render("druglist", { grouped, formatCurrency, formatName });
        break;
      case "json":
      default:
        res.json(records);
    }
  });

  app.get("/accounts/:id/druglist", async (req, res) => {});

  app.listen(config.get("port"), () => console.log("listening..."));
})();

function groupByFirstCharacter(objectArray, propertyName) {
  let list = objectArray.reduce((r, e) => {
    // get first letter of name of current element
    let group = e[propertyName][0];
    // if there is no property in accumulator with this letter create it
    if (!r[group]) r[group] = [e];
    // if there is push current element to children array for that letter
    else r[group].push(e);
    // return accumulator
    return r;
  }, {});
  return list;
}

function flattenDrugData(objectDrugData) {
  let tempArray = [];
  for (const prop in objectDrugData) {
    if (objectDrugData.hasOwnProperty(prop)) {
      tempArray.push({ drugName: prop });
      tempArray.push({});
      objectDrugData[prop].forEach((record) => {
        tempArray.push(record);
      });
    }
  }
  return tempArray;
}

function formatCurrency(d) {
  const fmt = new Intl.NumberFormat("en-US", {
    style: "currency",
    currency: "USD",
    minimumFractionDigits: 0,
  });
  return fmt.format(d);
}

function formatName(s) {
  return s.replace(/\//, "/\u200b");
}

async function isDirectory(dirpath) {
  try {
    const stat = await fsp.stat(dirpath);
    assert.ok(stat.isDirectory());
    return true;
  } catch {
    return false;
  }
}

async function isFile(filepath) {
  try {
    await fsp.access(filepath);
    const stat = await fsp.stat(filepath);
    assert.ok(stat.isFile());
    return true;
  } catch {
    return false;
  }
}

async function cacheAllAccountDrugLists(connectionPool) {
  const date = new Date();

  for await (const { accountId } of getAccounts(connectionPool)) {
    const cacheFileName = path.resolve(CACHE_DIR, `druglist_${accountId}.json`);
    const cacheExists = await isFile(cacheFileName);
    if (!cacheExists) {
      const result = getAccountDrugList(connectionPool, accountId, date);
      const records = [];
      for await (const row of result) {
        records.push(row);
      }
      await fsp.writeFile(cacheFileName, JSON.stringify(records));
    }
  }
}

function getAccounts(connectionPool) {
  const request = new sql.Request(connectionPool);
  request.query`
    SELECT
      accounts.accountId,
      MIN(accounts.accountName) AS accountName,
      MIN(accounts.goLiveDate) AS goLiveDate,
      COUNT('x') AS drugCount
    FROM Accounts accounts JOIN
      AccountFormularies accountFormularies ON accounts.accountId = accountFormularies.accountId JOIN
      DrugLists druglists ON accountFormularies.formularyId = druglists.formularyID JOIN
      Drugs drugs ON druglists.drugID = drugs.drugID
    WHERE	accounts.parentAccountId NOT IN (1305, 1140, 1067, 1092, 2421, 2336, 2110)
    GROUP	BY accounts.accountId
    HAVING COUNT('x') > 0
    ORDER BY accounts.accountId
  `;
  return streamQuery(request);
}

function getAccountDrugList(connectionPool, accountId, date) {
  const request = new sql.Request(connectionPool);
  request.query`
    SELECT
      drugs.drugName,
      drugs.copayAmount,
      drugs.exceptionPharmacyDrug
    FROM Accounts accounts JOIN
      AccountFormularies accountFormularies
        ON accounts.accountId = accountFormularies.accountId JOIN
      DrugLists druglists
        ON accountFormularies.formularyId = druglists.formularyID JOIN
      Drugs drugs
        ON druglists.drugID = drugs.drugID
    WHERE ${accountId} = accounts.accountId AND
      ${date} >= accounts.goLiveDate AND
      ${date} BETWEEN accountFormularies.effectiveDateStart AND accountFormularies.effectiveDateEnd AND
      ${date} BETWEEN druglists.effectiveDateStart AND druglists.effectiveDateEnd  AND
      ${date} BETWEEN drugs.effectiveDateStart AND drugs.effectiveDateEnd AND
      drugs.hideOnDrugList = 0
    ORDER BY drugName
  `;
  return streamQuery(request);
}

const streamQuery = (request, bufferThreshold = 10000) => {
  request.stream = true;

  const pullQueue = [];
  const pushQueue = [];
  let done = false;
  let complete = false;
  let paused = false;

  const pushValue = (args) => {
    if (pullQueue.length !== 0) {
      const resolver = pullQueue.shift();
      resolver({ done: false, value: args });
    } else {
      pushQueue.push(args);
      if (pushQueue.length > bufferThreshold) {
        paused = true;
        request.pause();
      }
    }
  };

  const pullValue = () => {
    return new Promise((resolve) => {
      if (pushQueue.length !== 0) {
        const args = pushQueue.shift();
        resolve({ done: false, value: args });
      } else if (complete) {
        done = true;
        request.off("row", doneHandler);
        request.off("done", rowHandler);
        resolve({ done });
      } else {
        pullQueue.push(resolve);
      }
      if (paused) {
        paused = false;
        request.resume();
      }
    });
  };

  const rowHandler = (args) => pushValue(args);
  const doneHandler = () => {
    complete = true;
    while (pullQueue.length !== 0) {
      const resolver = pullQueue.shift();
      const value = pushQueue.length > 0 ? pushQueue.shift() : undefined;
      done = value !== undefined;
      resolver({ done, value });
    }
  };

  request.on("row", rowHandler);
  request.on("done", doneHandler);

  return {
    [Symbol.asyncIterator]() {
      return this;
    },
    next: () => (done ? { done } : pullValue()),
    return: () => {
      done = true;
      request.off("row", rowHandler);
      request.off("done", doneHandler);
      return { done };
    },
    throw: (error) => {
      done = true;
      return {
        done,
        value: Promise.reject(error),
      };
    },
  };
};
